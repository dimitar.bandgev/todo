package out.of.box.task.service;

import out.of.box.task.exception.ApiException;
import out.of.box.task.model.CreateTask;
import out.of.box.task.model.Task;
import out.of.box.task.model.UpdateTask;

import java.util.List;

public interface TaskServiceInterface {

    List<Task> getAll();

    Task getOne(long id) throws ApiException;

    Task create(CreateTask taskDTO) throws ApiException;

    Task update(long id, UpdateTask taskDTO) throws ApiException;

    void delete(long id) throws ApiException;

}
