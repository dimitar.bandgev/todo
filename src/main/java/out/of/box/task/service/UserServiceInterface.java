package out.of.box.task.service;

import out.of.box.task.model.PerformUser;
import out.of.box.task.model.User;
import out.of.box.task.model.UserEntity;

import java.util.List;

public interface UserServiceInterface {

    List<User> getAll();

    User getUserDTO(long id);

    UserEntity getUserEntity(long id);

    User create(PerformUser userDTO);

    User update(long id, PerformUser userDTO);

    void delete(long id);

}
