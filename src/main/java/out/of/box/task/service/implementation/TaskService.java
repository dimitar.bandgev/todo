package out.of.box.task.service.implementation;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import out.of.box.task.exception.ApiException;
import out.of.box.task.model.*;
import out.of.box.task.model.enums.Status;
import out.of.box.task.repository.TaskRepository;
import out.of.box.task.service.TaskServiceInterface;

import java.util.List;

import static out.of.box.task.constants.WebContstants.Messages.INVALID_STATUS;
import static out.of.box.task.constants.WebContstants.Messages.TASK_NOT_FOUND;

@Service
public class TaskService implements TaskServiceInterface {

    private final ModelMapper modelMapper;
    private final UserService userService;
    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(ModelMapper modelMapper, UserService userService, TaskRepository taskRepository) {
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> getAll() {
        List<TaskEntity> taskEntities = taskRepository.findAll();

        return modelMapper.map(taskEntities, new TypeToken<List<Task>>() {
        }.getType());
    }

    @Override
    public Task getOne(long id) throws ApiException {
        TaskEntity taskEntity = taskRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, TASK_NOT_FOUND));

        return modelMapper.map(taskEntity, Task.class);
    }

    @Override
    @Transactional
    public Task create(CreateTask taskDTO) throws ApiException {
        checkStatus(taskDTO.getStatus());
        User userDTO = userService.getUserDTO(taskDTO.getUserId()); // only validation

        TaskEntity taskEntity = modelMapper.map(taskDTO, TaskEntity.class);
        taskEntity = taskRepository.saveAndFlush(taskEntity);

        return modelMapper.map(taskEntity, Task.class).user(userDTO);
    }

    @Override
    @Transactional
    public Task update(long id, UpdateTask taskDTO) throws ApiException {
        TaskEntity taskEntity = taskRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, TASK_NOT_FOUND));

        if (taskDTO.getUserId() != null) {
            UserEntity userEntity = userService.getUserEntity(taskDTO.getUserId()); // only validation
            taskEntity.setUserId(userEntity.getId());
            taskEntity.setUser(userEntity);
        }

        if (taskDTO.getName() != null)
            taskEntity.setName(taskDTO.getName());

        if (taskDTO.getDescription() != null)
            taskEntity.setDescription(taskDTO.getDescription());

        if (taskDTO.getStatus() != null)
            taskEntity.setStatus(checkStatus(taskDTO.getStatus()));

        taskRepository.flush();

        return modelMapper.map(taskEntity, Task.class);
    }

    @Override
    @Transactional
    public void delete(long id) throws ApiException {
        TaskEntity taskEntity = taskRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, TASK_NOT_FOUND));

        taskRepository.delete(taskEntity);
    }

    private Status checkStatus(String status) {
        try {
            return Status.valueOf(status);
        } catch (IllegalArgumentException e) {
            throw new ApiException(HttpStatus.BAD_REQUEST, INVALID_STATUS);
        }
    }
}
