package out.of.box.task.service.implementation;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import out.of.box.task.exception.ApiException;
import out.of.box.task.model.PerformUser;
import out.of.box.task.model.User;
import out.of.box.task.model.UserEntity;
import out.of.box.task.repository.UserRepository;
import out.of.box.task.service.UserServiceInterface;

import java.util.List;

import static out.of.box.task.constants.WebContstants.Messages.USER_ALREADY_EXIST;
import static out.of.box.task.constants.WebContstants.Messages.USER_NOT_FOUND;

@Service
public class UserService implements UserServiceInterface {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Autowired
    public UserService(ModelMapper modelMapper, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        List<UserEntity> userEntities = userRepository.findAll();

        return modelMapper.map(userEntities, new TypeToken<List<User>>() {
        }.getType());
    }

    @Override
    public User getUserDTO(long id) throws ApiException {
        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));

        return modelMapper.map(userEntity, User.class);
    }


    @Override
    public UserEntity getUserEntity(long id) throws ApiException {
        return userRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }

    @Override
    @Transactional
    public User create(PerformUser userDTO) {
        UserEntity isExist = userRepository.findByUsername(userDTO.getUsername());
        if (isExist != null) {
            throw new ApiException(HttpStatus.CONFLICT, USER_ALREADY_EXIST);
        }

        UserEntity userEntity = modelMapper.map(userDTO, UserEntity.class);
        userEntity = userRepository.saveAndFlush(userEntity);
        return modelMapper.map(userEntity, User.class);
    }

    @Override
    @Transactional
    public User update(long id, PerformUser userDTO) throws ApiException {
        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));

        UserEntity isExist = userRepository.findByUsername(userDTO.getUsername());
        if (isExist != null && isExist.getId() != userEntity.getId()) {
            throw new ApiException(HttpStatus.CONFLICT, USER_ALREADY_EXIST);
        }

        if (userDTO.getUsername() != null)
            userEntity.setUsername(userDTO.getUsername());

        userRepository.flush();

        return modelMapper.map(userEntity, User.class);
    }

    @Override
    @Transactional
    public void delete(long id) throws ApiException {
        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));

        userRepository.delete(userEntity);
    }

}
