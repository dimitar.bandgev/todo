package out.of.box.task.constants;

public class WebContstants {

    public static final String REST_CONTEX_STRING = "/ext";

    /**
     * Custom error messages
     */
    public static final class Messages {
        public static final String INVALID_STATUS = "Invalid status!";
        public static final String TASK_NOT_FOUND = "Task not found!";
        public static final String USER_NOT_FOUND = "User not found!";
        public static final String USER_ALREADY_EXIST = "User already exist!";
    }


}
