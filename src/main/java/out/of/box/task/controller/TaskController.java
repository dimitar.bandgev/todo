package out.of.box.task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import out.of.box.task.api.TaskApi;
import out.of.box.task.constants.WebContstants;
import out.of.box.task.model.CreateTask;
import out.of.box.task.model.Task;
import out.of.box.task.model.UpdateTask;
import out.of.box.task.service.implementation.TaskService;

import java.util.List;

@RestController
@RequestMapping(WebContstants.REST_CONTEX_STRING + "/v1/")
public class TaskController implements TaskApi {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public ResponseEntity<List<Task>> getAllTasks() {
        return ResponseEntity.ok().body(taskService.getAll());
    }

    @Override
    public ResponseEntity<Task> getTaskById(Long id) {
        return ResponseEntity.ok().body(taskService.getOne(id));
    }

    @Override
    public ResponseEntity<Task> createTask(CreateTask taskDTO) {
        // todo validation for the status, default value and also repair the enum in DB
        return ResponseEntity.ok().body(taskService.create(taskDTO));
    }

    @Override
    public ResponseEntity<Task> updateTaskById(Long id, UpdateTask taskDTO) {
        return ResponseEntity.ok().body(taskService.update(id, taskDTO));
    }

    @Override
    public ResponseEntity<Void> deleteTaskById(Long id) {
        taskService.delete(id);
        return ResponseEntity.ok().build();
    }

}
