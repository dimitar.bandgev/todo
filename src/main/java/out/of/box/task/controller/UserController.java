package out.of.box.task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import out.of.box.task.api.UserApi;
import out.of.box.task.constants.WebContstants;
import out.of.box.task.model.PerformUser;
import out.of.box.task.model.User;
import out.of.box.task.service.implementation.UserService;

import java.util.List;

@RestController
@RequestMapping(WebContstants.REST_CONTEX_STRING + "/v1/")
public class UserController implements UserApi {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok().body(userService.getAll());
    }

    @Override
    public ResponseEntity<User> getUserById(Long id) {
        return ResponseEntity.ok().body(userService.getUserDTO(id));
    }

    @Override
    public ResponseEntity<User> createUser(PerformUser user) {
        return ResponseEntity.ok().body(userService.create(user));
    }

    @Override
    public ResponseEntity<User> updateUserById(Long id, PerformUser user) {
        return ResponseEntity.ok().body(userService.update(id, user));
    }

    @Override
    public ResponseEntity<Void> deleteUserById(Long id) {
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

}
